import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.plaf.metal.MetalIconFactory.FolderIcon16;



public class Predict {

	
	int NO_OF_FILES;
	int noOfFiles(String folderpath) throws IOException
	{
		final File folder = new File(folderpath+"input\\");
		int count=0;
	    for (final File fileEntry : folder.listFiles()) {
	    	count++;
	    }	
	    //System.out.println("No of files are"+count);
	    return count;
	}
	
	
	int [][][] screentoken;
	int [][] prediction =new int[100][100];
	
	public void predict(String folderpath) throws IOException
	{
		NO_OF_FILES=noOfFiles(folderpath);
		screentoken=new int[NO_OF_FILES][100][100];
		//Folder location for all the files
		File folder = new File(folderpath+"input\\");
		int k=0;
	    for (final File fileEntry : folder.listFiles()) {
	    	
	    	System.out.println("Reading file"+fileEntry.getName());
	    	//CODE HERE TO READ THE PARTICULAR FILE NOW and SET THE PIXEL VALUES
	    	//*******************************
	    	File file = new File(folderpath+"input\\"+fileEntry.getName());
		    BufferedImage image = ImageIO.read(file);
			for (int y = 0; y < image.getHeight(); y++) {
			    for (int x = 0; x < image.getWidth(); x++) {	    		
			          int  clr   = image.getRGB(x, y); 
			          int  red   = (clr & 0x00ff0000) >> 16;
			          int  green = (clr & 0x0000ff00) >> 8;
			          int  blue  =  clr & 0x000000ff;
			          //System.out.print("For pixel position("+x+","+y+")");
			          //System.out.println(clr);
			          if(clr==-1) //WHITE
			          {
			        	  screentoken[k][x][y]=1; //1 represents a white
			          }
			          if(clr==-16777216)//BLACK PIXEL
			          {
			        	  screentoken[k][x][y]=0; //0 represents a black
			          }
			          //image.setRGB(x, y, clr);
			    }
			}
	    	//***********************************
			System.out.println(fileEntry.getName()+"read");
	        k++;    
	    }

	    
		// Tokens have been put in array now performing an AND operation
	    	for (int y = 0; y < 100; y++) {
	    		for (int x = 0; x < 100; x++) {
	    			int black=0;
	    	    	int white=0;
	    			for(k=0;k<NO_OF_FILES;k++)
	    		    {
	    		    
		    			if(screentoken[k][x][y]==1) //white
		    			{
		    				white++;
		    			}
		    			if(screentoken[k][x][y]==0)//black
		    			{
		    				black++;
		    			}
	    		    }
	    			System.out.println("--------");
	    			System.out.println("the no of whites for pixel("+x+","+y+")"+" are "+white);
	    			System.out.println("the no of blacks for pixel("+x+","+y+")"+" are "+black);
	    			float z= (float)white/(float)(white+black);
	    			System.out.println("The percentage is"+z);
	    			if(z>0.50) // no of white pixels is more
	    			{
	    				prediction[x][y]=1;
	    			}
	    			else //no of black pixels is more
	    			{
	    				prediction[x][y]=0;
	    			}
		    }
	    }
	    // Created Predicted User Token
	    createPredictedUserToken(folderpath);	
	    // Super Imposed Image
		createSuperImposedImagse(folderpath);
	}
	
	public void createPredictedUserToken(String folderpath ) throws IOException
	{
		File file1 = new File(folderpath+"output\\prediction.png");
	    final BufferedImage image1=new BufferedImage(100,100,BufferedImage.TYPE_3BYTE_BGR);
		int predicted[][]= new int [100][100];
		for (int y = 0; y < 100; y++) {
		    for (int x = 0; x < 100; x++) {	
		    	if(prediction[x][y]==1)
		    	{
		    		image1.setRGB(x, y, -1);
		    	}
		    	else
		    	{
		    		image1.setRGB(x, y, -16777216);
		    	}
		    }
		}
		ImageIO.write(image1,"png",file1);
	}
	
	public void createSuperImposedImagse(String folderpath) throws IOException
	{
		File folder = new File(folderpath+"input\\");
		int k=0;
	    for (final File fileEntry : folder.listFiles()) {
	    	System.out.println("Superimposing file "+fileEntry.getName()+" with file prediction.png");
	    	System.out.println("The value of k is"+k);
	    	
	    	File newfile = new File(folderpath+"output\\"+fileEntry.getName());
		    final BufferedImage image1=new BufferedImage(100,100,BufferedImage.TYPE_3BYTE_BGR);
		    for (int y = 0; y < 100; y++) {
			    for (int x = 0; x < 100; x++) {	
			    	if(screentoken[k][x][y]==1 && prediction[x][y]==1)
		    		{
		    		  image1.setRGB(x, y,-1);
		    		}
			    	/*else if(screentoken[k][x][y]==0 && prediction[x][y]==0)
			    	{
			    		image1.setRGB(x, y,-16777216);
			    	}*/
			    	else
			    	{
			    		image1.setRGB(x, y,-16777216/4);
			    	}
			    }
		    }
		    k++;
		    ImageIO.write(image1,"png",newfile);
	    }
	    
	}
	
	public static void main(String[] args) throws IOException {
		
		Predict p=new Predict();
		p.predict("C:\\Users\\ankur\\Desktop\\Howard\\");
		

		
	}

}
